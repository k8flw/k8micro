package k8micro;

import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.HttpStatus;

import javax.annotation.Nullable;

@Controller("/")
public class ProvaController {

    @Get
    public HttpStatus index() {
        return HttpStatus.OK;
    }

    @Get("/hello")
    public String hello(@Nullable String name) {
        if (name == null || name.isEmpty()) {
            name = "world";
        }
        return name;
    }
}